  var tag = document.createElement('script');
  
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  
  var player;
  
  function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      width: '100%',
      height: '100%',
      playerVars: {
        autoplay: 1,
        loop: 1,
        controls: 0,
        showinfo: 0,
        autohide: 1,
        modestbranding: 1,
        vq: 'hd1080'
      },
      videoId: "llfPNmYn2Vk",
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
  }
  
  function onPlayerReady(event) {
    event.target.playVideo();
    player.mute();
  }
  
  var done = false;
  
  function onPlayerStateChange(e) {
    var id = "llfPNmYn2Vk";
    if (e.data === YT.PlayerState.ENDED) {
      player.loadVideoById(id);
    }
  }
  
jQuery(document).ready(function ($) {
  
  
	$('#edit-submitted-telefon').mask("+7 (999) 999-9999");
	
	$('.btn-order, .btn-order-text').on('click', function() {
		$('.popup').addClass('active');
	});
	
	$('#block-block-2 .fa-search').on('click', function() {
		$('.search-wrapper').toggleClass('show');
	});

	$('.overlay, .close').on('click', function() {
		$('.popup').removeClass('active');
	});
	
    $('#block-block-20 .dropdown').each(function () {
        if ($(this).children('.item-list').length > 0) {
            $(this).find('>a').click(function (event) {
                event.preventDefault();
                $(this).toggleClass('opened').siblings('.item-list').slideToggle(300);
            }).append('<span class="caret"></span>');
        }
    });
    $('#block-block-20 .dropdown li.current').parent().parent().show();
    $('.tab-pane .view .more-link').each(function () {
        $(this).addClass('animated bounceIn').appendTo($(this).siblings('.view-content'));
    })
    $('.contact>*').not('h3').hide();
    $('.contact h3').click(function () {
        var x = $(this).siblings();
        x.slideToggle(600);
        $(this).toggleClass('down');
        if ($(this).hasClass('down')) {
            $(this).children('span').attr('class', 'glyphicon glyphicon-menu-down');
        } else {
            $(this).children('span').attr('class', 'glyphicon glyphicon-menu-right');
        }
    });
    $('.phone>span').click(function () {
        var val = $(this).attr('rel');
        var phone = val.replace(/\(?\)?\s?-?/g, '');
        phone = phone.substr(1);
        phone = '+7' + phone;
        $('#cur-phone').text(val);
        $('#block-block-2 .icon-phone-circled').attr('href', 'tel:' + phone);
    });
    var total = [];
    $('.average-rating>span:first-child').each(function () {
        total.push(parseFloat($(this).text()))
    });
    var val = 0;
    for (var i = 0; i < total.length; i++) {
        val += total[i];
    }
    ;val = val / total.length;
    $('#average-total').append(val.toFixed(2));
    $('.bootstrap-nav-wrapper>.nav-tabs').addClass('nav-justified');
    $(window).on('resize load', function () {
        if ($(window).width() < 500) {
            $('#logo').length > 0 ? $('#page-title').appendTo($('#block-block-14')) : '';
        } else if ($(window).width() > 500) {
            $('#logo').length > 0 ? $('#page-title').prependTo($('#content')) : '';
        }
        ;
    });
    
    $(".owl-carousel.otzivi").owlCarousel();
    $(".owl-carousel.team").owlCarousel({singleItem: true});
    
    $('.order-now').click(function(){
      var name = $(this).data('product'),
          price = $(this).data('price'),
          params = {
            name: name,
            price: price
          };
      
      yaCounter15942811.reachGoal("call_me_back", params ,function () {
        console.log(params , yaCounter15942811);
      });
      return true;
    });
    
/*
    Drupal.behaviors.MyCode = {
        attach: function (context, settings) {
            if ($(context).attr('id') == 'webform-client-form-797') {
                yaCounter15942811.reachGoal("forma");
            }
        }
    }
*/
});