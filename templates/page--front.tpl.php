<header id="header" role="banner" class="container-fluid navbar-fixed-top">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
    <span class="sr-only">Навигация</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
<?php if (!empty($primary_nav) || !empty($page['navigation'])): ?>
  <div class="navbar-collapse collapse">
    <a href="/" title="Главная" rel="home" id="logo">
      <img src="/sites/all/themes/sks/images/logo-horizontal.svg" alt="Главная">
    </a>
    <nav role="navigation">
      <?php if (!empty($primary_nav)):?>
        <?php print render($primary_nav); ?>
      <?php endif; ?>
    </nav>
    <?php if (!empty($page['navigation'])): ?>
      <?php print render($page['navigation']); ?>
    <?php endif; ?>
  </div>
<?php endif; ?>

  <div class="bg-dark-green search-wrapper">
    <div class="container">
      <?php
      $block = block_load('search', 'form');
      $block = _block_render_blocks(array($block));
      $block_build = _block_get_renderable_array($block);
/*       unset($block_build['search_form']['#theme_wrappers']); */
      echo drupal_render($block_build);
      ?>
    </div>
  </div>

</header>

  <?php print render($page['slides']); ?>
    	
  
  <div id="main" class="container">
  	      
    <div id="content" class="column" role="main">
    
      <div id="over_content" class="clearfix">
		<?php print render($page['over_content']); ?>
	  </div>
	  <?php print render($page['highlighted']); ?>
      <?php print $breadcrumb; ?>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
	  <div id="blocks">
		<?php print render($page['blocks']); ?>
	  </div>
      <?php //print $feed_icons; ?>
    </div><!-- /#content -->
	


    <?php
      // Render the sidebars to see if there's anything in them.
      $sidebar_first  = render($page['sidebar_first']);
      $sidebar_second = render($page['sidebar_second']);
    ?>

    <?php if ($sidebar_first || $sidebar_second): ?>
      <aside class="sidebars">
        <?php print $sidebar_first; ?>
        <?php print $sidebar_second; ?>
      </aside><!-- /.sidebars -->
    <?php endif; ?>

  </div><!-- /#main -->
</div>

<div class="container-fluid" style="padding:0">
	<?php print render($page['bottom']); ?>
	<?php print render($page['footer']); ?>
	
</div>

<div class="popup">
  <div class="close">×</div>
  <?php print render($page['popup']); ?>
</div>
<div class="overlay"></div>
